sudo apt-get update
sudo apt-get upgrade -y

# TODO add a check if the user is root
# TODO ask what the user want to install

#----------------------
#######RASP AP#########
#----------------------
# set timezone
RED='\033[0;31m' # Red
NC='\033[0m' # No Color
printf " ${RED}You have to set the right time zone for the Wifi in the next interface ${NC} \n"
sleep 5
sudo raspi-config

#install rasp ap
curl -sL https://install.raspap.com | bash

#----------------------
####Home Assistant#####
#----------------------
# install dependencies
sudo apt-get install -y python3 python3-dev python3-venv python3-pip bluez libffi-dev libssl-dev libjpeg-dev zlib1g-dev autoconf build-essential libopenjp2-7 libturbojpeg0-dev tzdata ffmpeg liblapack3 liblapack-dev libatlas-base-dev

# create homeassistant user
sudo useradd -rm homeassistant -G dialout,gpio,i2c

# create virtual environement 
sudo mkdir /srv/homeassistant
sudo chown homeassistant:homeassistant /srv/homeassistant
sudo -u homeassistant -H -s

# enter virtual environement
cd /srv/homeassistant
python3 -m venv .
source bin/activate

# run python package
python3 -m pip install wheel

# start homeassistant
hass &


#----------------------
#######Mosquitto#######
#----------------------

# install mosquitto
sudo apt install -y mosquitto mosquitto-clients
sudo systemctl enable mosquitto.service
systemctl status mosquitto
mosquitto -v

#create topic for home assistant
mosquitto_sub -d -t sensor/temperature
mosquitto_sub -d -t action/relay


#----------------------------------
########End of installation########
#----------------------------------
ip=$(hostname -I | awk '{print $1}')
echo -e "Installation complete"
echo -e "info for home assitant :"
echo -e "the ip adress of the home assistant webinterface is:http://$ip:8123"
echo -e "\n"
echo -e "info for rasp ap :\n"
echo "the ip adress of the rasp ap webinterface is:http://$ip"
echo "the default username is: admin"
echo "the default password is: secret"
echo "the default ssid is: raspi-webgui"
echo "the default password is: ChangeMe"
