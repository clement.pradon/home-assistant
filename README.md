# Home Assistant



## Installation

Executer le fichier install_ALL.sh :
```bash
    wget https://shorturl.at/AFKP0 -O install_all.sh
    chmod 777 ./install.sh
    ./install.sh
```

## Configuration

### Home Assistant

ajouter ces lignes à ```~/.homeassistant/configuration.yaml```

```
mqtt:
  sensor:
    - name: "Temperature"
      state_topic: "sensor/temperature"
      unit_of_measurement: "C°"
      value_template: "{{ value_json.temp }}"
    - name: "Humidité"
      state_topic: "sensor/temperature"
      unit_of_measurement: "%"
      value_template: "{{ value_json.hum }}"
  switch:
    - name: "Relay"
      command_topic: "action/relay"
      payload_on: "1"
      payload_off: "0"
      qos: 0
      retain: false
```

### Rasp Ap

Configuration d'internet : `/etc/raspap/networking`

Backup DHCP et PHP : `etc/raspap/bakcup`

Networking information : `/sys/class/net`

### Mosquitto 

Config de mosquitto : `/etc/mosquitto`